package Raumschiffe;

public class Ladung {
	private String bezeichnung;
	private int menge;
	
	Ladung (String b, int m) {
		this.bezeichnung = b;
		this.menge = m;
	}
	
	@Override
	public String toString() {
		return "Ladung [bezeichnung=" + bezeichnung + ", menge=" + menge + "]";
	}

	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	
}
