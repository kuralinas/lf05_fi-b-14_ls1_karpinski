import java.util.Scanner;

public class Schaltjahre {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		int jahr;
		
		System.out.println("Geben Sie ein Jahr an.");
		jahr = tastatur.nextInt();
		
		if(jahr % 4 == 0 && (jahr % 100 != 0 || jahr % 400 == 0)) {
			System.out.println("Schaltjahr");
		}else {
		System.out.println("Kein Schaltjahr");
		}
	}

}
