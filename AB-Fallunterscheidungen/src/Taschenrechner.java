import java.util.Scanner;

public class Taschenrechner {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		float z1, z2;
		char c;
		
		System.out.println("Geben Sie Zahl 1 ein: ");
		z1 = tastatur.nextFloat();
		
		System.out.println("Geben Sie Zahl 2 ein: ");
		z2 = tastatur.nextFloat();
		
		System.out.println("Geben Sie die Rechenart ein [+,-,*,/]");
		c = tastatur.next().charAt(0);
		
		try {
			if(c == '+') {
				System.out.println(add(z1,z2));
			}
			if(c == '-') {
				System.out.println(sub(z1,z2));
			}
			if(c == '*') {
				System.out.println(mul(z1,z2));
			}
			if(c == '/') {
				System.out.println(div(z1,z2));
			}
		} catch(Exception e) {
			System.out.println("Da ist was schiefgelaufen.");
		}
	}
	
	static float add(float z1, float z2) {
		return z1 + z2;
	}
	static float sub(float z1, float z2) {
		return z1 - z2;
	}
	static float mul(float z1, float z2) {
		return z1 * z2;
	}
	static float div(float z1, float z2) {
		return z1 / z2;
	}
}