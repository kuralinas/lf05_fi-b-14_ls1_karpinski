package train3;

public class Train3 {

	public static void main(String[] args) {
		int fahrzeit = 0;
		char haltInSpandau = 'n';
		char richtungHamburg = 'n';
		char haltInStendal = 'j';
		char endetIn = 'h';
		
		fahrzeit += 8; // Fahrzeit: Berlin Hbf. -> Spandau
		
		if (haltInSpandau == 'j') {
			fahrzeit += 2; // Halt in Spandau
		}
		if (richtungHamburg == 'n') {
			if (endetIn == 'w') {
				fahrzeit += 34;
				if (haltInStendal == 'j') {
					fahrzeit += 16;
				}
				else {
					fahrzeit += 6;
				}
				fahrzeit += 29;
				System.out.println("Sie erreichen Wolfsburg nach " + fahrzeit + " Minuten");
			}
			else if (endetIn == 'h') {
				fahrzeit += 34;
				if (haltInStendal == 'j') {
					fahrzeit += 16;
				}
				else {
					fahrzeit += 6;
				}
				fahrzeit += 63;
				System.out.println("Sie erreichen Hannover nach " + fahrzeit + " Minuten");
			}
			else if (endetIn == 'b') {
				fahrzeit += 34;
				if (haltInStendal == 'j') {
					fahrzeit += 16;
				}
				else {
					fahrzeit += 6;
				}
				fahrzeit += 50;
				System.out.println("Sie erreichen Braunschweig nach " + fahrzeit + " Minuten");
			}
		}
		else {
			fahrzeit += 96;
			System.out.println("Sie erreichen Hamburg nach " + fahrzeit + " Minuten");
		}
	}

}
