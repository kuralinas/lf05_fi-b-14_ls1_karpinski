package ungerade_zahlen;

public class UngeradeZahlen {

	public static void main(String[] args) {
		int[] zahlen = new int[20];
		
		for (int i = 0; i < 20; i++) {
			zahlen[i] = i;
			if (zahlen[i] % 2 != 0) {
				System.out.println(zahlen[i]);
			}
		}

	}

}
