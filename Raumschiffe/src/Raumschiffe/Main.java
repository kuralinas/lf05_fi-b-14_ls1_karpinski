package Raumschiffe;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		
		Ladung ladung1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladung2 = new Ladung("Borg-Schrott", 5);
		Ladung ladung3 = new Ladung("Rote Materie", 2);
		Ladung ladung4 = new Ladung("Forschungssonde", 35);
		Ladung ladung5 = new Ladung("Bat`leth Klingonen Schwert", 200);
		Ladung ladung6 = new Ladung("Plasma-Waffe", 50);
		Ladung ladung7 = new Ladung("Photonentorpedo", 3);
		
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh`ta", 2, new ArrayList<Ladung>(Arrays.asList(ladung1, ladung5)));
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, "IKW Khazara", 2, new ArrayList<Ladung>(Arrays.asList(ladung2, ladung3, ladung6)));
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni`Var", 5, new ArrayList<Ladung>(Arrays.asList(ladung7, ladung4)));
		System.out.println(klingonen.getLadung());
	}

}
