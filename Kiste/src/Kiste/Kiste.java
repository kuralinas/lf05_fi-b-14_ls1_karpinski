package Kiste;

public class Kiste {
	
	private float hoehe;
	private float tiefe;
	private float breite;
	private String farbe;

	public static void main(String[] args) {
		
		Kiste kiste1 = new Kiste(10, 10, 10, "Blau");
		Kiste kiste2 = new Kiste(12, 12, 12, "Lila");
		Kiste kiste3 = new Kiste(14, 14, 14, "Schwarz");
		
		System.out.println("Kiste1 : \n" + "Volumen:" + kiste1.getVolumen() + "\scm�\n" + "Farbe:" + kiste1.getFarbe());
		System.out.println("Kiste2 : \n" + "Volumen:" + kiste2.getVolumen() + "\scm�\n" + "Farbe:" + kiste2.getFarbe());
		System.out.println("Kiste3 : \n" + "Volumen:" + kiste3.getVolumen() + "\scm�\n" + "Farbe:" + kiste3.getFarbe());

	}
	
	Kiste(float h, float t, float b, String f) {
		this.breite = b;
		this.hoehe = h;
		this.tiefe = t;
		this.farbe = f;
	}
	
	public float getVolumen() {
		return hoehe * breite * tiefe;
	}
	
	public String getFarbe() {
		return this.farbe;
	}

}
