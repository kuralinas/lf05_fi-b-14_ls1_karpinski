package volumenberechnung;

import java.util.Scanner;

public class Volumenberechnung {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		String eingabe;
		Scanner tastatur = new Scanner(System.in);
		boolean ergebnis = true;
		
		System.out.println("Von welchem Objekt wollen sie das Volumen berechnen?\n Zur Auswahl stehen: \n(geben sie bitte den angezeigten Namen ein)\n"
				+ "W�rfel, Quader, Pyramide, Kugel");
		
		eingabe = tastatur.nextLine();
		
		while(ergebnis) {
			switch(eingabe) {
			case "W�rfel":
				float seiteA;
				System.out.println("Sie haben den W�rfel gew�hlt.");
				
				System.out.println("Geben sie bitte einen Wert f�r Seite A ein: ");
				seiteA = tastatur.nextFloat();
				
				System.out.println("Das Volumen f�r den W�rfel ist: " + volWuerfel(seiteA));
				ergebnis = false;
				break;
			case "Quader":
				float seiteA1, seiteB, seiteC;
				System.out.println("Sie haben den Quader gew�hlt.");
				
				System.out.println("Geben sie bitte einen Wert f�r Seite A ein: ");
				seiteA1 = tastatur.nextFloat();
				
				System.out.println("Geben sie bitte einen Wert f�r Seite B ein: ");
				seiteB = tastatur.nextFloat();
				
				System.out.println("Geben sie bitte einen Wert f�r Seite C ein: ");
				seiteC = tastatur.nextFloat();
				
				System.out.println("Das Volumen f�r den Quader ist: " + volQuader(seiteA1, seiteB, seiteC));
				ergebnis = false;
				break;
			case "Pyramide":
				float seiteA3, hoeheH;
				System.out.println("Sie haben die Pyramide gew�hlt.");
				
				System.out.println("Geben sie bitte einen Wert f�r Seite A ein: ");
				seiteA3 = tastatur.nextFloat();
				
				System.out.println("Geben sie bitte einen Wert f�r die H�he ein: ");
				hoeheH = tastatur.nextFloat();
				
				System.out.println("Das Volumen f�r die Pyramide ist: " + volPyramide(seiteA3, hoeheH));
				ergebnis = false;
				break;
			case "Kugel":
				float radiusR;
				System.out.println("Sie haben die Kugel gew�hlt.");
				
				System.out.println("Geben sie bitte einen Wert f�r den Radius ein: ");
				radiusR = tastatur.nextFloat();
				
				System.out.println("Das Volumen f�r die Kugel ist: " + volKugel(radiusR));
				ergebnis = false;
				break;
			default:
				System.out.println("'" + eingabe + "'" + "steht nicht zur Verf�gung. :(");
				System.out.println("Versuchen Sie es erneut: ");
				eingabe = tastatur.nextLine();
			}
		}
	}
	
	static float volWuerfel(float a) {
		return a * a * a;
	}
	
	static float volQuader(float a, float b, float c) {
		return a * b * c;
	}
	
	static float volPyramide(float a, float h) {
		return a * a * (h/3);
	}

	static float volKugel(float r) {
		return (float) ((4/3) * Math.pow(r, 3) * Math.PI);
	}
}
