package multiplikation;

import java.util.Scanner;

public class Multiplikation {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner tastatur = new Scanner(System.in);
		float wert1; 
		float wert2;
		float ergebnis;
		
	    System.out.println("Dieses Programm multipliziert zwei Zahlen miteinander.");
	    
	    System.out.println("Geben Sie Wert 1 ein: ");
	    wert1 = tastatur.nextFloat();
	    
	    System.out.println("Geben Sie jetzt bitte Wert 2 ein");
	    wert2 = tastatur.nextFloat();
	    
	    ergebnis = multiplikation(wert1, wert2);
	    
	    System.out.println("Das Ergibnis der Multiplikation ist: " + ergebnis);
	    
	}
	
	static float multiplikation(float x, float y) {
		return x * y;
	}
}
