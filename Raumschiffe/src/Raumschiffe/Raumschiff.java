package Raumschiffe;

import java.util.ArrayList;

public class Raumschiff {

		private int photonentorpedoAnzahl;
		private int energieversorgungInProzent;
		private int schildeInProzent;
		private int huelleInProzent;
		private int lebenserhaltungssystemeInProzent;
		private String schiffname;
		private int androidenAnzahl;
		private ArrayList<Ladung> ladungsverzeichnis;
		
		Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffname, int androidenAnzahl, ArrayList<Ladung> ladungsverzeichnis) {
			this.androidenAnzahl = androidenAnzahl;
			this.energieversorgungInProzent = energieversorgungInProzent;
			this.photonentorpedoAnzahl = photonentorpedoAnzahl;
			this.schildeInProzent = schildeInProzent;
			this.huelleInProzent = huelleInProzent;
			this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
			this.schiffname = schiffname;
			this.ladungsverzeichnis = ladungsverzeichnis;
		}
		
		public int getPhotonentorpedoAnzahl() {
			return photonentorpedoAnzahl;
		}
		public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
			this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		}
		public int getEnergieversorgungInProzent() {
			return energieversorgungInProzent;
		}
		public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
			this.energieversorgungInProzent = energieversorgungInProzent;
		}
		public int getSchildeInProzent() {
			return schildeInProzent;
		}
		public void setSchildeInProzent(int schildeInProzent) {
			this.schildeInProzent = schildeInProzent;
		}
		public int getHuelleInProzent() {
			return huelleInProzent;
		}
		public void setHuelleInProzent(int huelleInProzent) {
			this.huelleInProzent = huelleInProzent;
		}
		public int getLebenserhaltungssystemeInProzent() {
			return lebenserhaltungssystemeInProzent;
		}
		public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
			this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		}
		public String getSchiffname() {
			return schiffname;
		}
		public void setSchiffname(String schiffname) {
			this.schiffname = schiffname;
		}
		public int getAndroidenAnzahl() {
			return androidenAnzahl;
		}
		public void setAndroidenAnzahl(int androidenAnzahl) {
			this.androidenAnzahl = androidenAnzahl;
		}
		public ArrayList<Ladung> getLadung() {
			return ladungsverzeichnis;
		}
		public void addLadung(Ladung neueLadung) {
			this.ladungsverzeichnis.add(neueLadung);
		}

		@Override
		public String toString() {
			return "Raumschiff [photonentorpedoAnzahl=" + photonentorpedoAnzahl + ", energieversorgungInProzent="
					+ energieversorgungInProzent + ", schildeInProzent=" + schildeInProzent + ", huelleInProzent="
					+ huelleInProzent + ", lebenserhaltungssystemeInProzent=" + lebenserhaltungssystemeInProzent
					+ ", schiffname=" + schiffname + ", androidenAnzahl=" + androidenAnzahl + ", ladungsverzeichnis="
					+ ladungsverzeichnis + "]";
		}
		
}
