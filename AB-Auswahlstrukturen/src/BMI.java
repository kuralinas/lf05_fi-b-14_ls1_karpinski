import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		double kg;
		float gr;
		char ge;
		double bmi;
		
		System.out.println("Geben Sie ihr Gewicht ein.");
		kg = tastatur.nextFloat();
		
		System.out.println("Geben Sie nun ihre Gr��e ein.");
		gr = tastatur.nextFloat();
		
		System.out.println("Geben Sie ihr Geschlecht ein [m/w]");
		ge = tastatur.next().charAt(0);
		
		bmi = kg / (gr * gr);
		
		if(ge == 'm') {
			if(bmi < 20) {
				System.out.println("Du hast Untergewicht :(.");
			}
			if(bmi >= 20 && bmi <= 25) {
				System.out.println("Du hast Normalgewicht :).");
			}
			if(bmi > 25) {
				System.out.println("Du hast �bergewicht :(.");
			}
		}
		if(ge == 'w') {
			if(bmi < 19) {
				System.out.println("Du hast Untergewicht :(.");
			}
			if(bmi >= 19 && bmi <= 24) {
				System.out.println("Du hast Normalgewicht :).");
			}
			if(bmi > 24) {
				System.out.println("Du hast �bergewicht :(.");
			}
		}
	}
}
