package konfigurationschaos;

public class Konfigurationstest {

	public static void main(String[] args) {
		
		// AB 1
		int cent;
		float maximum;
		boolean booly = true;
		short shorty = -1000;
		float floaty = 1.255f;
		char charactery = '#';
		String stringy = "Ist das richtig?";
		final short CHECK_NR = 8765;
		
		
		cent = 70;
		cent = 80;
		maximum = 95.5f;
		
		/*
		 
		 �berlegen Sie sich Gr�nde, warum die Verwendung von Datentypen in Programmiersprachen sinnvoll ist.
		 
		 Somit muss nicht zur Laufzeit des Programms ein Typ ermittelt werden. Dadurch ist schon vor der Laufzeit klar wie viel Speicherplatz f�r die Variable n�tig ist.
		 Au�erdem ist es f�r die Lesbarkeit des Codes von Vorteil den Typen anzugeben, so wissen andere Programmierer wof�r die Variable gebraucht wird. (ein aussagekr�ftiger 
		 Name der Variable ist von Vorteil)
		  
		  
		 */
		
		// AB 2
		int ergebnis = 4 + 8 * 9 - 1;
		System.out.println(ergebnis);
		
		int zaehler;
		zaehler = 1;
		System.out.println(++zaehler);
		
		float ganzzahldivision;
		ganzzahldivision = 22.0f / 6.0f;
		System.out.println(ganzzahldivision);
		
		int schalter = 10;
		System.out.println(schalter > 7 && schalter < 12);
		
		System.out.println(schalter != 10 || schalter == 12);
		
		String oma = "Meine Oma\s" + "f�hrt im\s" + "H�hnerstall Motorrad.";
		System.out.println(oma);
	}

}
