package temperaturtabelle;

public class Temperaturtabelle {

	public static void main(String[] args) {
		System.out.printf( "%-12s", "Fahrenheit" );
		System.out.print( "|");
		System.out.printf( "%10s", "Celsius" );
		System.out.print( "\n-----------------------\n");
		System.out.printf( "%-12s", "-20" );
		System.out.print( "|");
		System.out.printf( "%10.2f", -28.8889 );
		System.out.printf( "\n%-12s", "-10" );
		System.out.print( "|");
		System.out.printf( "%10.2f", -23.3333 );
		System.out.printf( "\n%-12s", "+0" );
		System.out.print( "|");
		System.out.printf( "%10.2f", -17.7778 );
		System.out.printf( "\n%-12s", "+20" );
		System.out.print( "|");
		System.out.printf( "%10.2f", -6.6667 );
		System.out.printf( "\n%-12s", "+30" );
		System.out.print( "|");
		System.out.printf( "%10.2f", -1.1111 );
		
	}

}
